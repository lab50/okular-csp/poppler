// plugin.h
// © ООО «Лаборатория 50», 2022 <team@lab50.net>

#ifndef PLUGIN_H
#define PLUGIN_H

#include "goo/gfile.h"

#include <cassert>
#include <memory>
#include <string>
#include <vector>

struct Ref;
class GooString;
class X509CertificateInfo;
class XRef;

enum SignatureType
{
    nonGostSignature,
    unknownGostSignature,
    cadesBes,
    cadesXLongType1,
    cadesT
};

// UTF-8 strings are used used here.
class LicenseInfo
{
public:
    enum ParsingError
    {
        ok,
        licenseFileNotFound,
        cryptographicError,
        otherError
    };

    LicenseInfo() = default;
    virtual ~LicenseInfo() = default;

    virtual std::string id() const = 0;
    // User-friendly product name.
    virtual std::string product() const = 0;
    // User-friendly term.
    virtual std::string term() const = 0;
    virtual std::string name() const = 0;
    virtual std::string email() const = 0;
    virtual int number() const = 0;
    virtual bool isDesktopLicense() const = 0;
    virtual bool isServerLicense() const = 0;
    virtual bool isValid() const = 0;
    // A reason or an empty string.
    virtual std::string invalidReason() const = 0;
};

// Plugin interface.
class Plugin
{
public:
    Plugin() {}
    Plugin(const Plugin &other) = delete;
    virtual ~Plugin() = default;

    // Returns nullptr in case of a failure.
    // errorMsg is a user-friendly error message.
    virtual std::unique_ptr<LicenseInfo> loadLicense(LicenseInfo::ParsingError &errorCode, std::string &errorMsg) const = 0;
    inline std::unique_ptr<LicenseInfo> loadLicense(std::string &errorMsg) const
    {
        LicenseInfo::ParsingError errorCode;
        return loadLicense(errorCode, errorMsg);
    }

    // Arg path is an UTF-8 string.
    // Returns a path of a new file (UTF-8) or an empty string in case of a failure.
    virtual std::string installLicense(const std::string &path, std::string &errorMsg) const = 0;

    // Returns one-line user-friendly CSP description.
    virtual std::string getCspInfo() const noexcept = 0;

    // Permission checkers.
    virtual bool isValidDesktopLicense(std::string &errorMsg) const = 0;
    virtual bool isValidServerLicense(std::string &errorMsg) const = 0;

    // Returns: a list of available certificates.
    virtual std::vector<std::unique_ptr<X509CertificateInfo>> getCertificates() = 0;

    // Ruturns: a certificate or an empty pointer in case of a failure..
    virtual std::unique_ptr<X509CertificateInfo> getCertificate(const char *certificateId) = 0;

    // Creates a detached signature using CadES-X BES format.
    // Returns: signature bytes or an empty pointer in case of a failure.
    virtual std::unique_ptr<GooString> sign(
        const char *certificateId,
        const std::vector<unsigned char> &data,
        const Goffset expectedSignatureSize = 0,
        std::string *errorMsg = nullptr
    ) = 0;

    // Creates a detached signature using CadES-X Long Type 1 format.
    // Returns: signature bytes or an empty pointer in case of a failure.
    virtual std::unique_ptr<GooString> signUsingCadesXLongType1(
        const char *certificateId,
        const std::vector<unsigned char> &data,
        const wchar_t *tspUrl,
        const Goffset expectedSignatureSize = 0,
        std::string *errorMsg = nullptr
    ) = 0;

    // Checks a signature.
    // Args:
    //     signatureType: A signature type.
    //     signature: Signature bytes.
    //     data: Data to sign.
    // Returns: valid or not.
    virtual bool verify(
        SignatureType signatureType,
        const std::vector<unsigned char> &signature,
        const std::vector<unsigned char> &data
    ) = 0;

    // Creates a XObject using SVG.
    // Returns: ref to a new object or Ref::INVALID.
    virtual Ref embedSvgFromMemory(XRef *xref, const std::string &svgContent) = 0;

    // Returns SVG content of the default stamp.
    virtual std::string getDefaultStamp() const noexcept = 0;
};

// A plugin library should provide createPlugin() and deletePlugin().
typedef Plugin *(*create_plugin_t)(std::string &);
typedef void (*delete_plugin_t)(Plugin *);

// PluginHandle manages a Plugin instance and a corresponding shared library.
// Usage:
// auto plugin = PluginHandle::create(PluginHandle::CRYPTOPRO);
// (*plugin)->getCertificates();
class PluginHandle
{
protected:
    void *m_soHandle;
    Plugin *m_plugin;
    delete_plugin_t m_deletePlugin;

    PluginHandle(void *soHandle, Plugin *plugin, delete_plugin_t deletePlugin);

public:
#ifdef _WIN32
    static constexpr const char *CRYPTOPRO = "okularcsp-cryptopro.dll";
    static constexpr const char *JACARTA = "okularcsp-jacarta.dll";
#else
    static constexpr const char *CRYPTOPRO = "libokularcsp-cryptopro.so";
    static constexpr const char *JACARTA = "libokularcsp-jacarta.so";
#endif

    PluginHandle() = delete;
    PluginHandle(const PluginHandle &other) = delete;
    PluginHandle(PluginHandle &&other);
    ~PluginHandle();

    static std::unique_ptr<PluginHandle> create(const char *libName, std::string &errorMsg);

    inline Plugin* operator->()
    {
        return m_plugin;
    }
};

// Returns a lazy-initialized PluginHandle instance or a nullptr.
// You don't own the returned pointer.
PluginHandle POPPLER_PRIVATE_EXPORT *getPlugin();
PluginHandle POPPLER_PRIVATE_EXPORT *getPlugin(std::string &errorMsg);
#endif
