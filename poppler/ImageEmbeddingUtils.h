//========
//
// ImageEmbeddingUtils.h
//
// © ООО «Лаборатория 50», 2022 <team@lab50.net>
//
//=========//
//
// Modified under the Poppler project - http://poppler.freedesktop.org
//
// All changes made under the Poppler project to this file are licensed
// under GPL version 2 or later
//
// Author: Georgiy Sgibnev, 2021 <georgiy@lab50.net>

#ifndef IMAGE_EMBEDDING_UTILS_H
#define IMAGE_EMBEDDING_UTILS_H

#include <string>

#include "poppler_private_export.h"

class GooFile;
struct Ref;
class Object;
class XRef;

namespace ImageEmbeddingUtils {

// Creates a new base image (an object of type XObject referred to in a resource dictionary).
// Supported formats: PNG, JPEG.
// Args:
//     xref: Document's xref.
//     imageFile: An image file to embed.
// Returns ref to a new object or Ref::INVALID.
Ref POPPLER_PRIVATE_EXPORT embed(XRef *xref, const GooFile &imageFile);

// Same as above, but imagePath is a path to an image file.
Ref POPPLER_PRIVATE_EXPORT embed(XRef *xref, const std::string &imagePath);

// Returns a ratio (width / height) or a NaN value in case of a failure.
// Image XObjects and form XObjects are supported.
double POPPLER_PRIVATE_EXPORT getXObjectRatio(Object xObj);

}
#endif
