#include <config.h>

#include <mutex>
#include <dlfcn.h>

#include "Plugin.h"

PluginHandle::PluginHandle(void *soHandle, Plugin *plugin, delete_plugin_t deletePlugin)
  : m_soHandle(soHandle),
    m_plugin(plugin),
    m_deletePlugin(deletePlugin)
{
    assert(soHandle != nullptr);
    assert(plugin != nullptr);
    assert(deletePlugin != nullptr);
}

PluginHandle::PluginHandle(PluginHandle &&other)
  : m_soHandle(other.m_soHandle),
    m_plugin(other.m_plugin),
    m_deletePlugin(other.m_deletePlugin)
{
    other.m_soHandle = nullptr;
    other.m_plugin = nullptr;
    other.m_deletePlugin = nullptr;
}

PluginHandle::~PluginHandle()
{
    if ((m_plugin != nullptr) && (m_deletePlugin != nullptr)) {
        m_deletePlugin(m_plugin);
    }
    if (m_soHandle != nullptr) {
        dlclose(m_soHandle);
    }
}

static std::string troubleshootCades() noexcept
{
#ifdef _WIN32
    static const char *CADES_LIB = "cades.dll";
#else
    static const char *CADES_LIB = "libcades.so";
#endif

    void *libHandle = dlopen(CADES_LIB, RTLD_NOW);
    if (libHandle == nullptr) {
        return "недоступно стороннее ПО КриптоПро ЭЦП runtime";
    }
    dlclose(libHandle);
    return "";
}

std::unique_ptr<PluginHandle> PluginHandle::create(const char *libName, std::string &errorMsg)
{
    void *libHandle = dlopen(libName, RTLD_NOW);
    if (libHandle == nullptr) {
        errorMsg = troubleshootCades();
        if (errorMsg.empty()) {
            errorMsg = std::string("не удалось загрузить библиотеку ") + libName;
        }
        return nullptr;
    }

    create_plugin_t createPlugin = (create_plugin_t)dlsym(libHandle, "createPlugin");
    delete_plugin_t deletePlugin = (delete_plugin_t)dlsym(libHandle, "deletePlugin");
    if ((createPlugin == nullptr) || (deletePlugin == nullptr)) {
        dlclose(libHandle);
        errorMsg = std::string(libName) + " не содержит нужных функций";
        return nullptr;
    }

    Plugin *plugin = createPlugin(errorMsg);
    if (plugin == nullptr) {
        dlclose(libHandle);
        return nullptr;
    }
    errorMsg = "";
    return std::unique_ptr<PluginHandle>(new PluginHandle(libHandle, plugin, deletePlugin));
}

PluginHandle *getPlugin()
{
    std::string errorMsg;
    return getPlugin(errorMsg);
}

PluginHandle *getPlugin(std::string &errorMsg)
{
    static std::mutex mutex;
    static std::unique_ptr<PluginHandle> plugin;

    errorMsg = "";
    std::lock_guard<std::mutex> lock(mutex);
    if (!plugin) {
        plugin = PluginHandle::create(PluginHandle::CRYPTOPRO, errorMsg);
    }
    return plugin.get();
}
