//========
//
// FontEmbeddingUtils.h
//
// © ООО «Лаборатория 50», 2022 <team@lab50.net>
//
//=========//
//
// Modified under the Poppler project - http://poppler.freedesktop.org
//
// All changes made under the Poppler project to this file are licensed
// under GPL version 2 or later
//
// Author: Georgiy Sgibnev, 2021 <georgiy@lab50.net>

#ifndef FONT_EMBEDDING_UTILS_H
#define FONT_EMBEDDING_UTILS_H

#include <string>

#include "poppler_private_export.h"

class GooFile;
class GooString;
struct Ref;
class XRef;

namespace FontEmbeddingUtils {

// Inserts a new global font resource (an object of type "Font" referred to in a resource dictionary).
// Args:
//     xref: Document's xref.
//     fontFile: A font file to embed.
// Returns ref to a new object or Ref::INVALID.
Ref POPPLER_PRIVATE_EXPORT embed(XRef *xref, const GooFile &fontFile);

// Same as above, but fontPath is a path to a font file.
Ref POPPLER_PRIVATE_EXPORT embed(XRef *xref, const std::string &fontPath);

// Returns ref to a font resource or Ref::INVALID.
Ref findByName(XRef *xref, const GooString &name);

}

#endif
