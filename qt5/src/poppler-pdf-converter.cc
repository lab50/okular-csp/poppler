/* poppler-pdf-converter.cc: qt interface to poppler
 * Copyright (C) 2008, Pino Toscano <pino@kde.org>
 * Copyright (C) 2008, 2009, 2020, 2021, Albert Astals Cid <aacid@kde.org>
 * Copyright (C) 2020, Thorsten Behrens <Thorsten.Behrens@CIB.de>
 * Copyright (C) 2020, Klarälvdalens Datakonsult AB, a KDAB Group company, <info@kdab.com>. Work sponsored by Technische Universität Dresden
 * Copyright (C) 2021, Klarälvdalens Datakonsult AB, a KDAB Group company, <info@kdab.com>.
 * Copyright (C) 2021, Zachary Travis <ztravis@everlaw.com>
 * Copyright (C) 2021, Georgiy Sgibnev <georgiy@sgibnev.com>. Work sponsored by lab50.net.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street - Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "poppler-qt5.h"

#include "poppler-annotation-helper.h"
#include "poppler-annotation-private.h"
#include "poppler-private.h"
#include "poppler-converter-private.h"
#include "poppler-qiodeviceoutstream-private.h"

#include <QFile>
#include <QFileInfo>
#include <QUuid>

#include "Array.h"
#include "Form.h"
#include "UTF.h"
#include "Plugin.h"
#if defined(ENABLE_NSS3)
#    include "SignatureHandler.h"
#endif
#include "ImageEmbeddingUtils.h"
#include "FontEmbeddingUtils.h"
#include <ErrorCodes.h>

namespace Poppler {

class PDFConverterPrivate : public BaseConverterPrivate
{
public:
    PDFConverterPrivate();
    ~PDFConverterPrivate() override;

    PDFConverter::PDFOptions opts;
};

PDFConverterPrivate::PDFConverterPrivate() : BaseConverterPrivate(), opts(nullptr) { }

PDFConverterPrivate::~PDFConverterPrivate() = default;

PDFConverter::PDFConverter(DocumentData *document) : BaseConverter(*new PDFConverterPrivate())
{
    Q_D(PDFConverter);
    d->document = document;
}

PDFConverter::~PDFConverter() { }

void PDFConverter::setPDFOptions(PDFConverter::PDFOptions options)
{
    Q_D(PDFConverter);
    d->opts = options;
}

PDFConverter::PDFOptions PDFConverter::pdfOptions() const
{
    Q_D(const PDFConverter);
    return d->opts;
}

bool PDFConverter::convert()
{
    Q_D(PDFConverter);
    d->lastError = NoError;

    if (d->document->locked) {
        d->lastError = FileLockedError;
        return false;
    }

    QIODevice *dev = d->openDevice();
    if (!dev) {
        d->lastError = OpenOutputError;
        return false;
    }

    bool deleteFile = false;
    if (QFile *file = qobject_cast<QFile *>(dev))
        deleteFile = !file->exists();

    int errorCode = errNone;
    QIODeviceOutStream stream(dev);
    if (d->opts & WithChanges) {
        errorCode = d->document->doc->saveAs(&stream);
    } else {
        errorCode = d->document->doc->saveWithoutChangesAs(&stream);
    }
    d->closeDevice();
    if (errorCode != errNone) {
        if (deleteFile) {
            qobject_cast<QFile *>(dev)->remove();
        }
        if (errorCode == errOpenFile)
            d->lastError = OpenOutputError;
        else
            d->lastError = NotSupportedInputFileError;
    }

    return (errorCode == errNone);
}

// Embeds an SVG.
static Ref embedImage(::PDFDoc &doc, const std::string &svgContent, const X509CertificateInfo &certificate)
{
    if (svgContent.empty()) {
        return Ref::INVALID();
    }
    PluginHandle *plugin = getPlugin();
    if (plugin == nullptr) {
        return Ref::INVALID();
    }
    return (*plugin)->embedSvgFromMemory(
        doc.getXRef(),
        SignatureHandler::prepareSignatureText(svgContent, certificate)
    );
}

// Embeds an image file.
static Ref loadAndEmbedImage(::PDFDoc &doc, const QString &imageFilePath, const X509CertificateInfo &certificate)
{
    Ref imageResourceRef = Ref::INVALID();
    if (QFileInfo(imageFilePath).completeSuffix() == "svg") {
        imageResourceRef = embedImage(doc, loadFile(imageFilePath.toUtf8().constData()), certificate);
    } else {
        imageResourceRef = ImageEmbeddingUtils::embed(doc.getXRef(), imageFilePath.toUtf8().constData());
    }
    if (imageResourceRef == Ref::INVALID()) {
        qWarning() << "Couldn't embed " << imageFilePath.toUtf8().constData();
        return Ref::INVALID();
    }
    return imageResourceRef;
}

// Embeds a font file.
static Ref embedFont(::PDFDoc &doc, const QString &fontFilePath)
{
    Ref fontResourceRef = FontEmbeddingUtils::embed(doc.getXRef(), fontFilePath.toStdString());
    if (fontResourceRef == Ref::INVALID()) {
        qWarning() << "Couldn't embed " << fontFilePath.toUtf8().constData();
        return Ref::INVALID();
    }
    return fontResourceRef;
}

bool PDFConverter::sign(const NewSignatureData &data, QString &errorMsg)
{
#if defined(ENABLE_NSS3)
    Q_D(PDFConverter);
    d->lastError = NoError;

    if (d->document->locked) {
        d->lastError = FileLockedError;
        return false;
    }

    if (data.certNickname().isEmpty()) {
        qWarning() << "No certificate id given";
        return false;
    }
    auto certificate = SignatureHandler::getSigningCertificateById(data.certNickname().toUtf8().constData());
    if (!certificate) {
        qWarning().nospace() << "A certificate " << data.certNickname() << " not found";
        return false;
    }

    ::PDFDoc *doc = d->document->doc;
    ::Page *destPage = doc->getPage(data.page() + 1);
    Ref imageResourceRef = Ref::INVALID();
    if (!data.imageFilePath().isEmpty()) {
        imageResourceRef = loadAndEmbedImage(*doc, data.imageFilePath(), *certificate);
    } else if (data.imageFilePath().isEmpty() && data.signatureText().isEmpty()) {
        // Using default stamp!
        imageResourceRef = embedImage(*doc, getDefaultStamp().toStdString(), *certificate);
    }
    Ref fontResourceRef = Ref::INVALID();
    if (!data.fontFilePath().isEmpty()) {
        fontResourceRef = embedFont(*doc, data.fontFilePath());
    }

    std::string rawErrorMsg;
    std::unique_ptr<GooString> gSignatureText = std::unique_ptr<GooString>(utf8ToUtf16WithBom(SignatureHandler::prepareSignatureText(data.signatureText().toStdString(), *certificate)));
    std::unique_ptr<GooString> gSignatureLeftText = std::unique_ptr<GooString>(QStringToUnicodeGooString(data.signatureLeftText()));
    const auto reason = std::unique_ptr<GooString>(data.reason().isEmpty() ? nullptr : QStringToUnicodeGooString(data.reason()));
    const auto location = std::unique_ptr<GooString>(data.location().isEmpty() ? nullptr : QStringToUnicodeGooString(data.location()));
    bool res = doc->sign(d->outputFileName.toUtf8().constData(), data.certNickname().toUtf8().constData(), data.password().toUtf8().constData(), QStringToGooString(data.fieldPartialName()), data.page() + 1,
                         boundaryToPdfRectangle(destPage, data.boundingRectangle(), Annotation::FixedRotation), *gSignatureText, *gSignatureLeftText, data.fontSize(), convertQColor(data.fontColor()), data.borderWidth(),
                         convertQColor(data.borderColor()), convertQColor(data.backgroundColor()), reason.get(), location.get(),
                         data.digestName().toUtf8().constData(),
                         data.tspUrl().empty() ? nullptr : data.tspUrl().c_str(), imageResourceRef, fontResourceRef, &rawErrorMsg);
    if (!rawErrorMsg.empty()) {
        errorMsg = QString::fromUtf8(rawErrorMsg.c_str());
    }
    return res;
#else
    return false;
#endif
}

struct PDFConverter::NewSignatureData::NewSignatureDataPrivate
{
    NewSignatureDataPrivate() = default;

    QString certNickname;
    QString digestName = "SHA256";
    // Used for CAdES-X Long Type 1 signatures.
    std::wstring tspUrl;
    QString password;
    int page;
    QRectF boundingRectangle;
    QString signatureText;
    QString signatureLeftText;
    QString reason;
    QString location;
    QString imageFilePath = "";
    // A font file to embed.
    QString fontFilePath;
    double fontSize = 10.0;
    double leftFontSize = 20.0;
    QColor fontColor = Qt::red;
    QColor borderColor = Qt::red;
    double borderWidth = 1.5;
    QColor backgroundColor = QColor(240, 240, 240);

    QString partialName = QUuid::createUuid().toString();
};

PDFConverter::NewSignatureData::NewSignatureData() : d(new NewSignatureDataPrivate()) { }

PDFConverter::NewSignatureData::~NewSignatureData()
{
    delete d;
}

QString PDFConverter::NewSignatureData::certNickname() const
{
    return d->certNickname;
}

void PDFConverter::NewSignatureData::setCertNickname(const QString &certNickname)
{
    d->certNickname = certNickname;
}

QString PDFConverter::NewSignatureData::digestName() const
{
    return d->digestName;
}

void PDFConverter::NewSignatureData::setDigestName(const QString &digestName)
{
    d->digestName = digestName;
}

std::wstring PDFConverter::NewSignatureData::tspUrl() const
{
    return d->tspUrl;
}

void PDFConverter::NewSignatureData::setTspUrl(const std::wstring &url)
{
    d->tspUrl = url;
}

QString PDFConverter::NewSignatureData::password() const
{
    return d->password;
}

void PDFConverter::NewSignatureData::setPassword(const QString &password)
{
    d->password = password;
}

int PDFConverter::NewSignatureData::page() const
{
    return d->page;
}

void PDFConverter::NewSignatureData::setPage(int page)
{
    d->page = page;
}

QRectF PDFConverter::NewSignatureData::boundingRectangle() const
{
    return d->boundingRectangle;
}

void PDFConverter::NewSignatureData::setBoundingRectangle(const QRectF &rect)
{
    d->boundingRectangle = rect;
}

QString PDFConverter::NewSignatureData::signatureText() const
{
    return d->signatureText;
}

void PDFConverter::NewSignatureData::setSignatureText(const QString &text)
{
    d->signatureText = text;
}

QString PDFConverter::NewSignatureData::signatureLeftText() const
{
    return d->signatureLeftText;
}

void PDFConverter::NewSignatureData::setSignatureLeftText(const QString &text)
{
    d->signatureLeftText = text;
}

QString PDFConverter::NewSignatureData::reason() const
{
    return d->reason;
}

void PDFConverter::NewSignatureData::setReason(const QString &reason)
{
    d->reason = reason;
}

QString PDFConverter::NewSignatureData::location() const
{
    return d->location;
}

void PDFConverter::NewSignatureData::setLocation(const QString &location)
{
    d->location = location;
}

QString PDFConverter::NewSignatureData::imageFilePath() const
{
    return d->imageFilePath;
}

void PDFConverter::NewSignatureData::setImageFilePath(const QString &imageFilePath)
{
    d->imageFilePath = imageFilePath;
}

QString PDFConverter::NewSignatureData::fontFilePath() const
{
    return d->fontFilePath;
}

void PDFConverter::NewSignatureData::setFontFilePath(const QString &path)
{
    d->fontFilePath = path;
}

double PDFConverter::NewSignatureData::fontSize() const
{
    return d->fontSize;
}

void PDFConverter::NewSignatureData::setFontSize(double fontSize)
{
    d->fontSize = fontSize;
}

double PDFConverter::NewSignatureData::leftFontSize() const
{
    return d->leftFontSize;
}

void PDFConverter::NewSignatureData::setLeftFontSize(double fontSize)
{
    d->leftFontSize = fontSize;
}

QColor PDFConverter::NewSignatureData::fontColor() const
{
    return d->fontColor;
}

void PDFConverter::NewSignatureData::setFontColor(const QColor &color)
{
    d->fontColor = color;
}

QColor PDFConverter::NewSignatureData::borderColor() const
{
    return d->borderColor;
}

void PDFConverter::NewSignatureData::setBorderColor(const QColor &color)
{
    d->borderColor = color;
}

QColor PDFConverter::NewSignatureData::backgroundColor() const
{
    return d->backgroundColor;
}

double PDFConverter::NewSignatureData::borderWidth() const
{
    return d->borderWidth;
}

void PDFConverter::NewSignatureData::setBorderWidth(double width)
{
    d->borderWidth = width;
}

void PDFConverter::NewSignatureData::setBackgroundColor(const QColor &color)
{
    d->backgroundColor = color;
}

QString PDFConverter::NewSignatureData::fieldPartialName() const
{
    return d->partialName;
}

void PDFConverter::NewSignatureData::setFieldPartialName(const QString &name)
{
    d->partialName = name;
}

QString getDefaultStamp() noexcept
{
    PluginHandle *plugin = ::getPlugin();
    if (plugin == nullptr) {
        return "";
    }
    return QString::fromStdString((*plugin)->getDefaultStamp());
}

}
